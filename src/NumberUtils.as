package 
{
	public class NumberUtils
	{
		public function NumberUtils()
		{
		}
		public static function toFixedString(number:int, width:int):String {
			var ret:String = number.toString();
			while( ret.length < width ) 
				ret="0" + ret;
			return ret;
		}			
		public static function toFixedFloatString(number:Number, width:int, decimalLen:int = 2):String {
			var ret:String = number.toFixed(decimalLen); 
			while( ret.split(".")[0].length < width ) 
				ret="0" + ret;
			return ret;
		}			
		public static function setPrecision(number:Number, precision:int):Number {
			precision = Math.pow(10, precision); 
			return (Math.round(number * precision)/precision);
		}
	}
}